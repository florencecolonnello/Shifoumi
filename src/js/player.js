"use strict";
// export 
class Player {
	constructor(){
		this.score = 0;
	}

	// Renvoie le score
	getScore(){
		return this.score;
	}

	// incremente le score
	incrementScore(){
		this.score++;
	}
}
