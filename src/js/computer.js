"use strict";
//export 
class Computer {
	constructor(){
		this.score = 0;
		this.choices = ["Pierre", "Ciseaux", "Feuille"];
	}

	// Renvoie le choix aléatoire de l'ordinateur parmi 3 
	choose(){
		let index = this.getRandomInt(3);
		return this.choices[index];

	}

	// Renvoie le score
	getScore(){
		return this.score;
	}

	// incremente le score
	incrementScore(){
		this.score++;
	}

	// Renvoie un chiffre aléatoire entre 0 et max
	function(){
    getRandomInt(max) 
     return Math.floor(Math.random() * Math.floor(max));
    }
}
